# Xanadu (ChatClient)

by equokka aka Daniel Conceição

---

Fully compatible with netcat :)

Default port is 4000.

Host:
```sh
java -jar xanadu.jar [port]
```

Connect:
```sh
nc <hostname> <port>
```


